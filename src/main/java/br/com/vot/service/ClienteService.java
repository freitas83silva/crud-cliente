package br.com.vot.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import br.com.vot.domain.Cliente;
import br.com.vot.dto.ClienteDTO;
import br.com.vot.dto.DeleteEntity;

public interface ClienteService extends GenericService<Cliente, Long>{

	ResponseEntity<ClienteDTO> createCliente(@Valid ClienteDTO associado);

	ResponseEntity<ClienteDTO> updateCliente(@Valid ClienteDTO associado);

	ResponseEntity<DeleteEntity> deleteCliente(Long id);

	ResponseEntity<Page<ClienteDTO>> getAllCliente(Pageable pageable, Long filterId, String filterCpf);
}
