package br.com.vot.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.vot.domain.Cliente;
import br.com.vot.dto.ClienteDTO;
import br.com.vot.dto.DeleteEntity;
import br.com.vot.exception.EntityInvalidException;
import br.com.vot.exception.NotFoundException;
import br.com.vot.mapper.ClienteMapper;
import br.com.vot.repository.ClienteRepository;
import br.com.vot.service.ClienteService;

@Service
public class ClienteServiceImpl  extends GenericServiceImpl<Cliente, Long> implements ClienteService {

	private static final Logger log = LoggerFactory.getLogger(ClienteServiceImpl.class);
	
	@Autowired
	private ClienteRepository repository;
	
	ModelMapper model = new ModelMapper();
	
	private final ClienteMapper clienteMapper = new ClienteMapper();
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public ResponseEntity<ClienteDTO> createCliente(@Valid ClienteDTO cliente) {
		
		Cliente in = repository.findByCpf(cliente.getCpf());
		Cliente returnCliente = model.map(cliente, Cliente.class);
		
		if(in == null) {
			
			returnCliente = repository.save(returnCliente);
			ClienteDTO dto = model.map(returnCliente, ClienteDTO.class);
			return ResponseEntity.ok(dto);
		}
		throw new NotFoundException(messagesService.get("cliente.cpf.exist"));
	}

	@Override
	public ResponseEntity<ClienteDTO> updateCliente(@Valid ClienteDTO cliente) {
		
		Cliente in = repository.findFirstById(cliente.getId());
		Cliente returnCliente = model.map(cliente, Cliente.class);
		
		if(in != null) {
			
			returnCliente = repository.save(returnCliente);
			ClienteDTO dto = model.map(returnCliente, ClienteDTO.class);
			return ResponseEntity.ok(dto);	
		}
		throw new EntityInvalidException(messagesService.get("cliente.not.exist"));
	}

	@Override
	public ResponseEntity<DeleteEntity> deleteCliente(Long id) {
		
		Cliente in = repository.findFirstById(id);	
		
		if(in != null) {
			repository.deleteById(id);
			DeleteEntity inD = new DeleteEntity();
			inD.setStatus("Cliente excluida");
			return ResponseEntity.ok(inD);
		}
		throw new EntityInvalidException(messagesService.get("cliente.not.exist"));
	}
	
	@Override
	public ResponseEntity<Page<ClienteDTO>> getAllCliente(Pageable pageable, Long filterId, String filterCpf) {
		
        Page<ClienteDTO> clienteDTO = null;
        Page<ClienteDTO> pageAllDTO= null;
		List<ClienteDTO> content = new ArrayList<>();
		
		if(filterId != null) {
			Page<Cliente> pageId = repository.findFirstById(filterId, pageable);
			clienteDTO = clienteMapper.convertToSliceDTO(pageId);
			
			clienteDTO.forEach(cont->{
				ClienteDTO cli = new ClienteDTO();
				cli.setId(cont.getId());
				cli.setName(cont.getName());
				cli.setCpf(cont.getCpf());
				cli.setDtnasc(cont.getDtnasc());
				
			    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			    Date dataNascimento;
				try {
					dataNascimento = sdf.parse(cont.getDtnasc().toString());
				    int idade = calculaIdade(dataNascimento);					
					cli.setIdade(idade);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				
				content.add(cli);
			});
			
		} else if(filterCpf != null) {
			Page<Cliente> pageId = repository.findByCpf(filterCpf, pageable);
			clienteDTO = clienteMapper.convertToSliceDTO(pageId);
			
			clienteDTO.forEach(cont->{
				ClienteDTO cli = new ClienteDTO();
				cli.setId(cont.getId());
				cli.setName(cont.getName());
				cli.setCpf(cont.getCpf());
				cli.setDtnasc(cont.getDtnasc());
				
			    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			    Date dataNascimento;
				try {
					dataNascimento = sdf.parse(cont.getDtnasc().toString());
				    int idade = calculaIdade(dataNascimento);					
					cli.setIdade(idade);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				
				content.add(cli);
			});
			
		} else {
			Page<Cliente> pageAll = repository.findAll(pageable);
			clienteDTO = clienteMapper.convertToSliceDTO(pageAll);
			
			clienteDTO.forEach(cont->{
				ClienteDTO cli = new ClienteDTO();
				cli.setId(cont.getId());
				cli.setName(cont.getName());
				cli.setCpf(cont.getCpf());
				cli.setDtnasc(cont.getDtnasc());
				
			    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			    Date dataNascimento;
				try {
					dataNascimento = sdf.parse(cont.getDtnasc().toString());
				    int idade = calculaIdade(dataNascimento);					
					cli.setIdade(idade);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				
				content.add(cli);
			});
		}
		
		pageAllDTO = new PageImpl<>(content, clienteDTO.getPageable(), clienteDTO.getTotalElements());
		
		return ResponseEntity.ok(pageAllDTO);
	}
	
	public int calculaIdade(Date dataNasc) {

	    Calendar dataNascimento = Calendar.getInstance();  
	    dataNascimento.setTime(dataNasc); 
	    Calendar hoje = Calendar.getInstance();  

	    int idade = hoje.get(Calendar.YEAR) - dataNascimento.get(Calendar.YEAR); 

	    if(hoje.get(Calendar.MONTH) < dataNascimento.get(Calendar.MONTH)) {
	    	idade--;  
	    } else { 
	        if(hoje.get(Calendar.MONTH) == dataNascimento.get(Calendar.MONTH) && hoje.get(Calendar.DAY_OF_MONTH) < dataNascimento.get(Calendar.DAY_OF_MONTH)) {
	            idade--; 
	        }
	    }

	    return idade;
	}
}