package br.com.vot.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.data.domain.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.vot.domain.Cliente;
import br.com.vot.dto.ClienteDTO;
import br.com.vot.dto.DeleteEntity;
import br.com.vot.service.ClienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "Cliente Controller")
@RestController
@RequestMapping(value = "/cliente")
public class ClienteController {
    
	private final ClienteService service;

    public ClienteController(ClienteService service) {
        this.service = service;
    }
    
    @PostMapping(value = "create")
    @ApiOperation(value = "Create new Cliente")
    public ResponseEntity<ClienteDTO> createCliente(
            @ApiParam(value = "Objeto cliente", required = true)
            @RequestBody @Valid ClienteDTO cliente) {
    	
    	return service.createCliente(cliente);
    }
    
    @PutMapping(value = "update")
    @ApiOperation(value = "Update new Cliente")
    public ResponseEntity<ClienteDTO> updateCliente(
            @ApiParam(value = "Objeto cliente", required = true)
            @RequestBody @Valid ClienteDTO cliente) {
    	
    	return service.updateCliente(cliente);
    }
    
    @DeleteMapping(value = "{id}")
    @ApiOperation(value = "Delete Cliente pelo Id")
    public ResponseEntity<DeleteEntity> deleteCliente(
            @ApiParam(value = "Id da cliente", required = true) @PathVariable Long id) {
        return service.deleteCliente(id);
    }
    
    @GetMapping(value = "pagination")
    @ApiOperation(value = "List All Cliente paginated by cpf or Id")
    public ResponseEntity<Page<ClienteDTO>> getAllPaginated(
    		@RequestParam(value = "page", defaultValue = "0", required = false) int page,
            @RequestParam(value = "count", defaultValue = "10", required = false) int count,
            @RequestParam(value = "order", defaultValue = "ASC", required = false) Sort.Direction direction,
            @RequestParam(value = "sort", defaultValue = "name", required = false) String sortProperty,
            @RequestParam(value = "filterId", required = false) Long filterId,
            @RequestParam(value = "filterCpf", required = false) String filterCpf){
    	
		if(count == 0 && page == 0) {
			List<Cliente> total = service.getAll();
			int countAll = total.size();
			count = countAll;
		}
		
        Pageable pageable = PageRequest.of(page, count, Sort.by(direction, sortProperty));
        return service.getAllCliente(pageable, filterId, filterCpf);
    }    
}
