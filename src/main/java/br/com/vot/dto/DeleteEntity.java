package br.com.vot.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeleteEntity {
	
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
