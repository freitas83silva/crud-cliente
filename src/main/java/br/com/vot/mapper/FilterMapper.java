package br.com.vot.mapper;


public interface FilterMapper<E, F> {
    E convertFilterToEntity(F filterDTO);
}