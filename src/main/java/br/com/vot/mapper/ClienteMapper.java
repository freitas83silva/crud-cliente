package br.com.vot.mapper;

import br.com.vot.domain.Cliente;
import br.com.vot.dto.ClienteDTO;

public class ClienteMapper implements GenericMapper<Cliente, ClienteDTO>{

	@Override
	public ClienteDTO convertToDTO(Cliente entity) {
		ClienteDTO dto = new ClienteDTO();
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setCpf(entity.getCpf());
		dto.setDtnasc(entity.getDtnasc());
		
		return dto;
	}

	@Override
	public Cliente convertToEntity(ClienteDTO dto) {
		Cliente entity = new Cliente();
		entity.setId(dto.getId());
		entity.setName(dto.getName());
		entity.setCpf(dto.getCpf());
		entity.setDtnasc(dto.getDtnasc());
		
		return entity;
	}

}
