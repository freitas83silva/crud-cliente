package br.com.vot.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.vot.domain.Cliente;

@Repository
public interface ClienteRepository  extends JpaRepository<Cliente, Long> {

	Cliente findFirstById(Long id);
	
	Cliente findByCpf(String cpf);

	Page<Cliente> findFirstById(Long filterId, Pageable pageable);

	Page<Cliente> findByCpf(String filterCpf, Pageable pageable);

}
