package br.com.vot.factory;

import br.com.vot.dto.ClienteDTO;

public class CreateClienteDTOTest {
    public static ClienteDTO get(Long id, String name, String cpf){
        return new ClienteDTO(){
            {
                setId(id);
                setName(name);
                setCpf(cpf);
            }
        };
    }
}
