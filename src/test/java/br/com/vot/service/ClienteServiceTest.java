package br.com.vot.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.vot.domain.Cliente;
import br.com.vot.dto.ClienteDTO;
import br.com.vot.factory.CreateClienteDTOTest;
import br.com.vot.repository.ClienteRepository;
import br.com.vot.service.impl.ClienteServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class ClienteServiceTest {

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }
    
    @InjectMocks
    private ClienteServiceImpl service;
    
    @Mock
	private ClienteRepository associadoRepository;
    
    ModelMapper model = new ModelMapper();
    
    private final static Long idAss = 1L;
    private final static String nameAss = "PEDRO AUGUSTO";
    private final static String cpf = "64935345004";
    
    @Test
    public void createAssociadoTest() {
    	ClienteDTO dto = CreateClienteDTOTest.get(null, nameAss, cpf);
    	Cliente returnAssociado = model.map(dto, Cliente.class);
    	Cliente outputEntity = returnAssociado;
        outputEntity.setId(idAss);
        
        when(associadoRepository.save(returnAssociado)).thenReturn(outputEntity);
        
        ClienteDTO dtoReturn = model.map(outputEntity, ClienteDTO.class);
        
        assertEquals(outputEntity, associadoRepository.save(returnAssociado));
    }
    
    @Test
    public void updateAssociadoTest() {
    	ClienteDTO dto = CreateClienteDTOTest.get(idAss, nameAss, cpf);
    	Cliente returnAssociado = model.map(dto, Cliente.class);
    	Cliente outputEntity = returnAssociado;
    	
        when(associadoRepository.save(returnAssociado)).thenReturn(outputEntity);
        assertEquals(outputEntity, service.update(returnAssociado));
    }
    
    @Test
    public void getAllTest() {
    	
    }
    
    @Test(expected=NullPointerException.class)
    public void deleteClienteTest() {
    	
	    service.deleteCliente(idAss);
	     
	    verify(service,times(1)).deleteCliente(idAss); 
    }
}
